clear;
clc;

N_DEZENAS = 60;
SENA = 6;
QUINA = 5;
QUADRA = 4;

printf("--- S E N A ---\n");
tipo = SENA;
for i=6:15  
  nComb_sn = (nchoosek(N_DEZENAS,SENA)) / (nchoosek(i,tipo) );      
  printf("jogando %1.0i numeros: 1:%i\n", i, round(nComb_sn));  
endfor

printf("--- Q U I N A ---\n");
tipo = QUINA;
for i=6:15  
  nComb_qn = (nchoosek(N_DEZENAS,SENA)) / (nchoosek(i,tipo)*nchoosek(N_DEZENAS-i,SENA-tipo) );
  printf("jogando %1.0i numeros: 1:%i\n", i, round(nComb_qn));
endfor

printf("--- Q U A D R A ---\n");
tipo = QUADRA;
for i=6:15  
  nComb_qd = (nchoosek(N_DEZENAS,SENA)) / (nchoosek(i,tipo)*nchoosek(N_DEZENAS-i,SENA-tipo) );
  printf("jogando %1.0i numeros: 1:%i\n", i, round(nComb_qd));
endfor
